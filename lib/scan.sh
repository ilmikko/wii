bars() {
	awk -F/ '{
bar = "[   ]";
if ($1 < -90) {
	bar = "[.  ]";
} else if ($1 < -75) {
	bar = "[.: ]";
} else {
	bar = "[.:I]";
}
print(bar"/"$2);
}';
}

match() {
	matches="$(echo "$known" | grep -i "$1")";
	match_count="$(echo "$matches" | wc -l)";
	[ -n "$matches" ] || match_count=0;

	if [ "$match_count" -gt 0 ]; then
		if [ "$match_count" -gt 1 ]; then
			log "Multiple matches found:";
			log "$matches";
			return 1;
		fi
		echo "$matches";
		return 0;
	fi
}

match_ssid() {
	known="$(list_known_ssids)";
	
	LOG=$(match "^$1$") || return 1;
	test -n "$LOG" && echo "$LOG" && return 0;
	LOG=$(match "$1") || return 1;
	test -n "$LOG" && echo "$LOG" && return 0;

	known="$(scan | awk -F/ '{ print $2 }')";

	LOG=$(match "^$1$") || return 1;
	test -n "$LOG" && echo "$LOG" && return 0;
	LOG=$(match "$1") || return 1;
	test -n "$LOG" && echo "$LOG" && return 0;

	exit 1;
}

list_known_ssids() {
	interface=$(get_interface);
	for file in "$PROFILE_DIR/"*; do
		[ -f "$file" ] || continue;
		basename "$file" | awk '/^'"$interface"'/ { sub(/^'"$interface"'-?/, "", $0); print($0) }';
	done
}

scan() {
	root ip link set "$interface" up;
	root iw "$interface" scan | awk '
function register(ssid, bssid, signal) {
	if (!known[ssid]) {
		list[count++] = ssid;
		known[ssid] = 1;
		bssids[ssid] = bssid;
		signals[ssid] = signal;
	} else if (signal < signals[ssid]) {
		bssids[ssid] = bssid;
		signals[ssid] = signal;
	}
}
/^\s*signal:/ { gsub(/^\s*signal:\s*|\s*dBm\s*$/, "", $0); signal=$0 }
/^\s*SSID:/ { gsub(/^\s*SSID:\s*|\s*$/, "", $0); ssid=$0 }
/^BSS/ { gsub(/^BSS\s*|\(.*$/, "", $0); bssid=$0; if (ssid) register(ssid, bssid, signal); }
END {
	register(ssid, bssid, signal);
	
	for (i = 0; i < count; i++) {
		ssid = list[i];
		signal = signals[ssid];
		bssid = bssids[ssid];
		print(signal"/"ssid);
	}
}' | sort | bars;
}
