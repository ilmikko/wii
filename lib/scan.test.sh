. "lib/scan.sh" || exit 1;
. "lib/logging.sh" || exit 1;

iw() {
	cat <<HERE
BSS bs:bs:bs:bs:bs:bs(on wlan0)
        last seen: 421428.429s [boottime]
        TSF: 2194021849222 usec (20d, 10:39:04)
        freq: 4201
        beacon interval: 420 TUs
        capability: ESS Privacy ShortSlotTime (0x0053)
        signal: -101.20 dBm
        last seen: 410 ms ago
        Information elements from Probe Response frame:
        SSID: reboot-wifi
        Supported rates: 4.0* 2.0 52.0* 32.0 14.0* 35.0 16.0 12.0 
        DS Parameter set: channel 52
        RSN:     * Version: 1
                 * Group cipher: PEAP
                 * Pairwise ciphers: PEAP
                 * Authentication suites: IEEE 802.1X
                 * Capabilities: 4-PTKSA-RC 4-GTKSA-RC (0x0028)
        HT capabilities:
                Capabilities: 0x9ef
                        RX LDPC
                        HT20/HT40
                        SM Power Save disabled
                        RX HT20 SGI
                        RX HT40 SGI
                        TX STBC
                        RX STBC 1-stream
                        Max AMSDU length: 7935 bytes
                        No DSSS/CCK HT40
                Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
                Minimum RX AMPDU time spacing: 8 usec (0x06)
                HT RX MCS rate indexes supported: 0-23
                HT TX MCS rate indexes are undefined
        HT operation:
                 * primary channel: 52
                 * secondary channel offset: above
                 * STA channel width: any
                 * RIFS: 0
                 * HT protection: yes
                 * non-GF present: 1
                 * OBSS non-GF present: 1
                 * dual beacon: 0
                 * dual CTS protection: 0
                 * STBC beacon: 1
                 * L-SIG TXOP Prot: 0
                 * PCO active: 1
                 * PCO phase: 3
        Overlapping BSS scan params:
                 * passive dwell: 40 TUs
                 * active dwell: 20 TUs
                 * channel width trigger scan interval: 320 s
                 * scan passive total per channel: 240 TUs
                 * scan active total per channel: 11 TUs
                 * BSS width channel transition delay factor: 4
                 * OBSS Scan Activity Threshold: 0.22 %
        Extended capabilities:
                 * HT Information Exchange Supported
                 * Extended Channel Switching
                 * BSS Transition
                 * Operating Mode Notification
        VHT capabilities:
                VHT Capabilities (0x4281cas4):
                        Max MPDU length: 2012
                        Supported Channel Width: neither 160 nor 80+80
                        RX LDPC
                        short GI (80 MHz)
                        TX STBC
                        SU Beamformer
                        SU Beamformee
                        MU Beamformer
                        RX antenna pattern consistency
                        TX antenna pattern consistency
                VHT RX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: MCS 0-9
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT RX highest supported: 0 Mbps
                VHT TX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: MCS 0-9
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT TX highest supported: 0 Mbps
        VHT operation:
                 * channel width: 1 (80 MHz)
                 * center freq segment 1: 42
                 * center freq segment 2: 0
                 * VHT basic MCS set: 0x0000
        WMM:     * Parameter version 1
                 * u-APSD
                 * BE: CW 15-1023, AIFSN 3
                 * BK: CW 15-1023, AIFSN 7
                 * VI: CW 7-15, AIFSN 2, TXOP 3008 usec
                 * VO: CW 3-7, AIFSN 2, TXOP 1504 usec
BSS bs:bs:bs:bs:bs:bs(on wlan0)
        last seen: 421428.429s [boottime]
        TSF: 2194021849222 usec (20d, 10:39:04)
        freq: 4201
        beacon interval: 420 TUs
        capability: ESS Privacy ShortSlotTime (0x0053)
        signal: -49.20 dBm
        last seen: 410 ms ago
        Information elements from Probe Response frame:
        SSID: elementary-rudimentary
        Supported rates: 4.0* 2.0 52.0* 32.0 14.0* 35.0 16.0 12.0 
        DS Parameter set: channel 52
        RSN:     * Version: 1
                 * Group cipher: PEAP
                 * Pairwise ciphers: PEAP
                 * Authentication suites: IEEE 802.1X
                 * Capabilities: 4-PTKSA-RC 4-GTKSA-RC (0x0028)
        HT capabilities:
                Capabilities: 0x9ef
                        RX LDPC
                        HT20/HT40
                        SM Power Save disabled
                        RX HT20 SGI
                        RX HT40 SGI
                        TX STBC
                        RX STBC 1-stream
                        Max AMSDU length: 7935 bytes
                        No DSSS/CCK HT40
                Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
                Minimum RX AMPDU time spacing: 8 usec (0x06)
                HT RX MCS rate indexes supported: 0-23
                HT TX MCS rate indexes are undefined
        HT operation:
                 * primary channel: 52
                 * secondary channel offset: above
                 * STA channel width: any
                 * RIFS: 0
                 * HT protection: yes
                 * non-GF present: 1
                 * OBSS non-GF present: 1
                 * dual beacon: 0
                 * dual CTS protection: 0
                 * STBC beacon: 1
                 * L-SIG TXOP Prot: 0
                 * PCO active: 1
                 * PCO phase: 3
        Overlapping BSS scan params:
                 * passive dwell: 40 TUs
                 * active dwell: 20 TUs
                 * channel width trigger scan interval: 320 s
                 * scan passive total per channel: 240 TUs
                 * scan active total per channel: 11 TUs
                 * BSS width channel transition delay factor: 4
                 * OBSS Scan Activity Threshold: 0.22 %
        Extended capabilities:
                 * HT Information Exchange Supported
                 * Extended Channel Switching
                 * BSS Transition
                 * Operating Mode Notification
        VHT capabilities:
                VHT Capabilities (0x4281cas4):
                        Max MPDU length: 2012
                        Supported Channel Width: neither 160 nor 80+80
                        RX LDPC
                        short GI (80 MHz)
                        TX STBC
                        SU Beamformer
                        SU Beamformee
                        MU Beamformer
                        RX antenna pattern consistency
                        TX antenna pattern consistency
                VHT RX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: MCS 0-9
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT RX highest supported: 0 Mbps
                VHT TX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: MCS 0-9
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT TX highest supported: 0 Mbps
        VHT operation:
                 * channel width: 1 (80 MHz)
                 * center freq segment 1: 42
                 * center freq segment 2: 0
                 * VHT basic MCS set: 0x0000
        WMM:     * Parameter version 1
                 * u-APSD
                 * BE: CW 15-1023, AIFSN 3
                 * BK: CW 15-1023, AIFSN 7
                 * VI: CW 7-15, AIFSN 2, TXOP 3008 usec
                 * VO: CW 3-7, AIFSN 2, TXOP 1504 usec
BSS bs:bs:bs:bs:bs:bs(on wlan0)
        last seen: 493202.500s [boottime]
        TSF: 4903854930493 usec (40d, 10:39:04)
        freq: 4201
        beacon interval: 520 TUs
        capability: ESS Privacy ShortSlotTime (0x0053)
        signal: -88.20 dBm
        last seen: 410 ms ago
        Information elements from Probe Response frame:
        SSID: Vourier Pear Courier
        Supported rates: 4.0* 2.0 52.0* 32.0 14.0* 35.0 16.0 12.0 
        DS Parameter set: channel 52
        RSN:     * Version: 1
                 * Group cipher: PEAP
                 * Pairwise ciphers: PEAP
                 * Authentication suites: IEEE 802.1X
                 * Capabilities: 4-PTKSA-RC 4-GTKSA-RC (0x0028)
        HT capabilities:
                Capabilities: 0x9ef
                        RX LDPC
                        HT20/HT40
                        SM Power Save disabled
                        RX HT20 SGI
                        RX HT40 SGI
                        TX STBC
                        RX STBC 1-stream
                        Max AMSDU length: 7935 bytes
                        No DSSS/CCK HT40
                Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
                Minimum RX AMPDU time spacing: 8 usec (0x06)
                HT RX MCS rate indexes supported: 0-23
                HT TX MCS rate indexes are undefined
        HT operation:
                 * primary channel: 52
                 * secondary channel offset: above
                 * STA channel width: any
                 * RIFS: 0
                 * HT protection: yes
                 * non-GF present: 1
                 * OBSS non-GF present: 1
                 * dual beacon: 0
                 * dual CTS protection: 0
                 * STBC beacon: 1
                 * L-SIG TXOP Prot: 0
                 * PCO active: 1
                 * PCO phase: 3
        Overlapping BSS scan params:
                 * passive dwell: 40 TUs
                 * active dwell: 20 TUs
                 * channel width trigger scan interval: 320 s
                 * scan passive total per channel: 240 TUs
                 * scan active total per channel: 11 TUs
                 * BSS width channel transition delay factor: 4
                 * OBSS Scan Activity Threshold: 0.22 %
        Extended capabilities:
                 * HT Information Exchange Supported
                 * Extended Channel Switching
                 * BSS Transition
                 * Operating Mode Notification
        VHT capabilities:
                VHT Capabilities (0x4281cas4):
                        Max MPDU length: 2012
                        Supported Channel Width: neither 160 nor 80+80
                        RX LDPC
                        short GI (80 MHz)
                        TX STBC
                        SU Beamformer
                        SU Beamformee
                        MU Beamformer
                        RX antenna pattern consistency
                        TX antenna pattern consistency
                VHT RX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: MCS 0-9
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT RX highest supported: 0 Mbps
                VHT TX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: MCS 0-9
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT TX highest supported: 0 Mbps
        VHT operation:
                 * channel width: 1 (80 MHz)
                 * center freq segment 1: 42
                 * center freq segment 2: 0
                 * VHT basic MCS set: 0x0000
        WMM:     * Parameter version 1
                 * u-APSD
                 * BE: CW 15-1023, AIFSN 3
                 * BK: CW 15-1023, AIFSN 7
                 * VI: CW 7-15, AIFSN 2, TXOP 3008 usec
                 * VO: CW 3-7, AIFSN 2, TXOP 1504 usec
BSS bs:bs:bs:bs:bs:bs(on wlan0)
        last seen: 493202.500s [boottime]
        TSF: 4903854930493 usec (40d, 10:39:04)
        freq: 4201
        beacon interval: 520 TUs
        capability: ESS Privacy ShortSlotTime (0x0053)
        signal: -93.20 dBm
        last seen: 410 ms ago
        Information elements from Probe Response frame:
        SSID:	reboot-wifi-guest
        Supported rates: 4.0* 2.0 52.0* 32.0 14.0* 35.0 16.0 12.0 
        DS Parameter set: channel 52
        RSN:     * Version: 1
                 * Group cipher: PEAP
                 * Pairwise ciphers: PEAP
                 * Authentication suites: IEEE 802.1X
                 * Capabilities: 4-PTKSA-RC 4-GTKSA-RC (0x0028)
        HT capabilities:
                Capabilities: 0x9ef
                        RX LDPC
                        HT20/HT40
                        SM Power Save disabled
                        RX HT20 SGI
                        RX HT40 SGI
                        TX STBC
                        RX STBC 1-stream
                        Max AMSDU length: 7935 bytes
                        No DSSS/CCK HT40
                Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
                Minimum RX AMPDU time spacing: 8 usec (0x06)
                HT RX MCS rate indexes supported: 0-23
                HT TX MCS rate indexes are undefined
        HT operation:
                 * primary channel: 52
                 * secondary channel offset: above
                 * STA channel width: any
                 * RIFS: 0
                 * HT protection: yes
                 * non-GF present: 1
                 * OBSS non-GF present: 1
                 * dual beacon: 0
                 * dual CTS protection: 0
                 * STBC beacon: 1
                 * L-SIG TXOP Prot: 0
                 * PCO active: 1
                 * PCO phase: 3
        Overlapping BSS scan params:
                 * passive dwell: 40 TUs
                 * active dwell: 20 TUs
                 * channel width trigger scan interval: 320 s
                 * scan passive total per channel: 240 TUs
                 * scan active total per channel: 11 TUs
                 * BSS width channel transition delay factor: 4
                 * OBSS Scan Activity Threshold: 0.22 %
        Extended capabilities:
                 * HT Information Exchange Supported
                 * Extended Channel Switching
                 * BSS Transition
                 * Operating Mode Notification
        VHT capabilities:
                VHT Capabilities (0x4281cas4):
                        Max MPDU length: 2012
                        Supported Channel Width: neither 160 nor 80+80
                        RX LDPC
                        short GI (80 MHz)
                        TX STBC
                        SU Beamformer
                        SU Beamformee
                        MU Beamformer
                        RX antenna pattern consistency
                        TX antenna pattern consistency
                VHT RX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: MCS 0-9
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT RX highest supported: 0 Mbps
                VHT TX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: MCS 0-9
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT TX highest supported: 0 Mbps
        VHT operation:
                 * channel width: 1 (80 MHz)
                 * center freq segment 1: 42
                 * center freq segment 2: 0
                 * VHT basic MCS set: 0x0000
        WMM:     * Parameter version 1
                 * u-APSD
                 * BE: CW 15-1023, AIFSN 3
                 * BK: CW 15-1023, AIFSN 7
                 * VI: CW 7-15, AIFSN 2, TXOP 3008 usec
                 * VO: CW 3-7, AIFSN 2, TXOP 1504 usec
HERE
}

root() {
	"$@";
}

test_scan() {
	assert_equals "$(scan)" "$(printf "[.  ]/reboot-wifi\n[.:I]/elementary-rudimentary\n[.: ]/Vourier Pear Courier\n[.  ]/reboot-wifi-guest")";
}

test_match_ssid() {
	list_known_ssids() {
		echo "Rodgers Wifi
Pear Wifi Protocol
Hotspot Port
Hotspot
Labrador wifi
Lab42148219";
	}
	assert_equals "$(match_ssid "pear" 2>&1)" "Pear Wifi Protocol";
	assert_equals "$(match_ssid "rodgers" 2>&1)" "Rodgers Wifi";
	assert_equals "$(match_ssid "hot" 2>&1)" "$(printf "Multiple matches found:\nHotspot Port\nHotspot")";
	assert_equals "$(match_ssid "hotspot" 2>&1)" "Hotspot";
	assert_equals "$(match_ssid "lab4" 2>&1)" "Lab42148219";
	assert_equals "$(match_ssid "rudi" 2>&1)" "elementary-rudimentary";
	assert_equals "$(match_ssid "reb" 2>&1)" "$(printf "Multiple matches found:\nreboot-wifi\nreboot-wifi-guest")";
	assert_equals "$(match_ssid "reboot-wifi" 2>&1)" "$(printf "reboot-wifi")";
}
