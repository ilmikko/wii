daemon-restart() {
if ! systemctl status --no-pager "netctl-auto@$interface" | grep --silent "active (running)"; then
	root netctl restart "$(basename "$1")";
else
	root systemctl restart "netctl-auto@$interface";
fi
}
