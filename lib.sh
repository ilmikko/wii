fail() {
	log "$@";
	exit 1;
}

get_interface() {
	ip -brief link | awk '/^wl/ { print $1 }';
}

root() {
	sudo "$@";
}

reset() {
	root killall wpa_supplicant;
	interface="$(get_interface)";
	root ip link set $interface down;
	daemon-restart "no-file";
}

PROFILE_DIR="/etc/netctl/";

main() {
	interface=$(get_interface);
	while [ $# -gt 0 ]; do
		case $1 in
			--*)
				fail "Unknown command: $1";
				;;
			c|co|con|conn|conne|connec|connect)
				shift;
				[ -n "$1" ] || fail "Where do you want to connect?";
				ssid="$(match_ssid "$1")";
				[ -n "$ssid" ] || fail "Could not find a network with SSID matching to $1.";
				connect "$ssid";
				;;
			r|re|res|rese|reset)
				reset;
				;;
			s|sc|sca|scan)
				scan;
				;;
			*)
				fail "Unknown action: $1";
				;;
		esac
		shift;
	done
}
